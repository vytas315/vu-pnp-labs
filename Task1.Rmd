---
title: "Project 1 Tasks 1 - 3"
output:
  pdf_document: default
  html_notebook: default
---
```{r echo=T, results='hide', message=FALSE}
library(ggplot2)
library(dplyr)  # data manipulations including tibble
library(skimr)  # skimming of data
```

# Economist data on sero-surveys
Data used is from a review The Economist completed on sero-surveys around the world. Their data was shared on github here:
https://github.com/TheEconomist/Grim-Tallies

```{r}
sero <- read.csv("sero-surveys.csv")
head(sero)
```

# Data from reliability engineering analysis

Data is from a practice data set used in Practical Applications of Bayesian Reliability by Abeyratne and Liu, 2019. It contains a series of tests and their failure times. I also used analysis done in this blog for inspiration: https://www.r-bloggers.com/2020/01/survival-analysis-fitting-weibull-models-for-improving-device-reliability-in-r/

```{r}
data_reliability <- read.csv(file = "Example3.1Data.txt", header = FALSE) %>% as_tibble()
head(data_reliability)
data_tbl <- data_reliability %>% rename(fatigue_duration = V1)
data_tbl %>% skim()
```


```{r}
colnames(sero)
```

## Interesting parameters
Analysis will begin with a focus on USA data. We will analyze positive sero (anti-body) tests completed between March and June. There are 26 observations.

```{r}
sero[sero$iso3c=="USA",]
```
```{r}
usa_data = sero[sero$iso3c=="USA",]
usa_data$date = as.Date(usa_data$date, '%Y-%m-%d')
usa_data$sero_ir_per_100k = usa_data$sero_ir *100000
usa_sero_plot <- ggplot(data = usa_data, mapping = aes(x = date, y = sero_ir_per_100k)) +
geom_point()
usa_sero_plot
```
## Distribution
The distribution looks quite random with little correlation in time. Unclear what distribution this data takes. Since the task asks us to estimate an exponential distribution, that is what I will do.

```{r}
# Task 1.1
set.seed(12)# random numbers will be taken from the 12th place of #the random numbers generator
eta <- 10
nu <- 2
n <- 100
t <- eta*(((-1)*log(1-runif(n)))^(1/nu))# generates T_i t
```

## Task 1.2a
### Computing Maximum Likelihood (ML) using nlm
Function is the loglikelihood of the exponential density function. It takes parameters $\eta$ and $\nu$.

```{r}
# Task 1.2a
my_data = sero[sero$iso3c=="USA","sero_ir"]*100000  # US sero rate per 100,000, 26 observations from March to June
nlm_function_to_min <- function(par) {
(-1)* sum (log(par[2])-log(par[1])+(par[2]-1)*(log(my_data)-log(par[1]))-(my_data/par[1])^par[2])
} # minus loglikelihood function
rez <- nlm(nlm_function_to_min, theta <- c(11,1), hessian=TRUE)

rez$estimate #ML estimator of $\eta$ and $\nu$
rez$hessian

```

### Computing Maximum Likelihood (ML) using maxLik

```{r}
library(maxLik)
ln_l2 <- function(par) {
eta_iv <- par[1]
nu_iv <- par[2]
sum (log(par[2])-log(par[1])+(par[2]-1)*(log(my_data)-log(par[1]))-(my_data/par[1])^par[2])} # loglikelihood function
mle <- maxLik(logLik = ln_l2, start = c(eta_iv=10,nu_iv = 1))
coef(mle) # estimator of the parameter lambda
hessian(mle) # estimator of the second derivative of the loglikelihood function.

```
We find very similar estimates for both eta and nu. Interestingly, when using nlm, a seed value of 10 for eta yielded completely different results. Using a variety of values, it appeared that only 10 gave the strange result. Unclear why.

### Computing Maximum Likelihood (ML) using optim
```{r}
ln_l2 <- function(par) {
eta_iv <- par[1]
nu_iv <- par[2]
sum (log(par[2])-log(par[1])+(par[2]-1)*(log(my_data)-log(par[1]))-(my_data/par[1])^par[2])} # loglikelihood function
rez_optim<-optim(c(10,1), ln_l2,
method="L-BFGS-B", lower = c(1.0e-13,1.0e-13), upper=c(100000,100),control=list(fnscale=-1),hessian=TRUE)# function optim
rez_optim$par # estimator of lambda
rez_optim$hessian # estimator of the second derivative of the loglikelihood function

```
Again, we see very similar estimates.

## Task 1.2b

```{r}
S_iv <- pweibull(c(2,5,10,15), shape=rez$estimate[2], scale = rez$estimate[1], lower.tail = FALSE) # estimators of the survival function at points 2,5,10,15.
eta<- 10
nu<- 2
x<- seq(from = 0, to = 16, by = 0.1)
S_teor <- pweibull(x, shape=nu, scale = eta, lower.tail = FALSE)
S_iv <- pweibull(x, shape=rez$estimate[2], scale = rez$estimate[1], lower.tail = FALSE)
grafikas <- data.frame(x,S_teor,S_iv)
plot(grafikas$x,grafikas$S_teor,type="l",col="red", xlab ="x",ylab="S") 
lines(grafikas$x,grafikas$S_iv,col="green")
```

Since my data doesn't follow the distribution, doing the rest using the originally suggested data.
```{r}
eta <- 10
nu <- 2
n <- 100
t <- eta*(((-1)*log(1-runif(n)))^(1/nu))# generates T_i
ln_l1 <- function(par) {
(-1)* sum (log(par[2])-log(par[1])+(par[2]-1)*(log(t)-log(par[1]))-(t/par[1])^par[2])
} # minus loglikelihood function
# eta=par[1]; nu=par[2];
rez <- nlm(ln_l1, theta <- c(10,1), hessian=TRUE) # function nlm #(non-linear minimization) minimizes the minus loglikelihood function,
# so it maximizes the loglikelihood function.
rez$estimate # ML estimators of the parameters eta and nu.
rez$hessian <- (-1)*rez$hessian # estimator of the matrix of second order # derivatives of the loglikelihood function.
rez$hessian

ln_l2 <- function(par) {
eta_iv <- par[1]
nu_iv <- par[2]
sum (log(par[2])-log(par[1])+(par[2]-1)*(log(t)-log(par[1]))-(t/par[1])^par[2])} # loglikelihood function
rez_optim<-optim(c(10,1), ln_l2,
method="L-BFGS-B", lower = c(1.0e-13,1.0e-13), upper=c(100000,100),control=list(fnscale=-1),hessian=TRUE)# function optim
rez_optim$par # estimator of lambda
rez_optim$hessian # estimator of the second derivative of the loglikelihood function

S_iv <- pweibull(c(2,5,10,15), shape=rez$estimate[2], scale = rez$estimate[1], lower.tail = FALSE) # estimators of the survival function at points 2,5,10,15.
# If the ML estimator of the c.d.f. is computed, then lower.tail = FALSE is not needed. 
S_iv
eta<- 10
nu<- 2
x<- seq(from = 0, to = 16, by = 0.1)
S_teor <- pweibull(x, shape=nu, scale = eta, lower.tail = FALSE)
S_iv <- pweibull(x, shape=rez$estimate[2], scale = rez$estimate[1], lower.tail = FALSE)
grafikas <- data.frame(x,S_teor,S_iv) 
plot(grafikas$x,grafikas$S_teor,type="l",col="red", xlab ="x",ylab="S") 
lines(grafikas$x,grafikas$S_iv,col="green")
```

As you might expect, now that the data is built from a Weibull distribution, it matches quite well.

```{r}
eta_iv=rez$estimate[1]
nu_iv=rez$estimate[2]
x_p_iv <- qweibull(c(0.1,0.5,0.9), shape=nu_iv, scale =
eta_iv, lower.tail = TRUE) # point estimates
x_p_iv
```

## Task 1.2c

Finding confidence intervals.

```{r}
eta_iv=rez$estimate[1] #estimator of eta
nu_iv=rez$estimate[2] #estimator of nu
p <- 0.5 # for median p=0.5 should be taken.
x_p_iv <- qweibull(p, shape=nu_iv, scale =eta_iv, lower.tail = TRUE) # point estimate of
# p-quantile (median, in particular). 
Q <- 0.95 #confidence level
alpha <- 1-Q
#alternative way - find derivatives of gamma(eta,nu) by hand: u <- (-1)*log(1-p) #(notation)
#J<- c(1/eta_iv, (-1)/(nu_iv*nu_iv)*log(u))
library(Deriv)
gam_th <- function(x, y) log(x)+log(-log(0.5))/y # function gamma(eta,nu) #for delta method
J<- c(Deriv(gam_th)(eta_iv,nu_iv)[1],Deriv(gam_th)(eta_iv,nu_iv)[2]) 
J
# the order of derivatives has to match the order of parameters in #the optimization function
names(J) <- NULL
Fisher_inf=-rez_optim$hessian
sigma_kv_Q <- t(J)%*%solve(Fisher_inf/n)%*%J # t(J)nmeans transposed matrix J, # solve(Fisher_inf/n) is the inverse of Fisher_inf/n
sigma_Q <- sqrt(sigma_kv_Q)
kr_r <- qnorm(alpha/2, mean = 0, sd = 1, lower.tail = FALSE) #alpha/2 critical # value z_{alpha/2} of the standard normal law
x_p_l <- x_p_iv*exp((-1)*sigma_Q*kr_r/sqrt(n)) # lower confidence bound
x_p_u <- x_p_iv*exp(sigma_Q*kr_r/sqrt(n)) # upper confidence bound
x_p_teor <- qweibull(p, shape=nu, scale = eta, lower.tail = TRUE) 
data.frame(p, x_p_teor, x_p_iv, Q, x_p_l, x_p_u)

```

Now repeated using the deltamethod in the msm package.

```{r}
library(msm)
eta_iv=rez_optim$par[1] #estimator of eta
nu_iv=rez_optim$par[2] #estimator of nu
p <- 0.5 # for median p=0.5 should be taken Q <- 0.95 #confidence level
alpha <- 1-Q
Fisher_inf<- (-1)*rez_optim$hessian
Fisher_inv<-solve(Fisher_inf)
sd_gamma <- deltamethod(list(~ log(x1)+log(-log(0.5))/x2), rez_optim$par, Fisher_inv)
sd_gamma
kr_r <- qnorm(alpha/2, mean = 0, sd = 1, lower.tail = FALSE) #alpha/2 critical # value z_{alpha/2} of the standard normal law
x_p_iv <- qweibull(p, shape=nu_iv, scale =
eta_iv, lower.tail = TRUE) # point estimate of
#p-quantile (median, in particular).
x_p_l <- x_p_iv*exp((-1)*sd_gamma*kr_r) # lower confidence bound
x_p_u <- x_p_iv*exp(sd_gamma*kr_r) # upper confidence bound 
x_p_teor <- qweibull(p, shape=nu, scale = eta, lower.tail = TRUE) 
data.frame(p, x_p_teor, x_p_iv, Q, x_p_l, x_p_u)
```

## Task 1.2d

Now we repeat for the 0.1 quantile.

```{r}
library(msm)
eta_iv=rez_optim$par[1] #estimator of eta
nu_iv=rez_optim$par[2] #estimator of nu
p <- 0.1 # for median p=0.5 should be taken Q <- 0.95 #confidence level
alpha <- 1-Q
Fisher_inf<- (-1)*rez_optim$hessian
Fisher_inv<-solve(Fisher_inf)
sd_gamma <- deltamethod(list(~ log(x1)+log(-log(0.5))/x2), rez_optim$par, Fisher_inv)
sd_gamma
kr_r <- qnorm(alpha/2, mean = 0, sd = 1, lower.tail = FALSE) #alpha/2 critical # value z_{alpha/2} of the standard normal law
x_p_iv <- qweibull(p, shape=nu_iv, scale =
eta_iv, lower.tail = TRUE) # point estimate of
#p-quantile (median, in particular).
x_p_l <- x_p_iv*exp((-1)*sd_gamma*kr_r) # lower confidence bound
x_p_u <- x_p_iv*exp(sd_gamma*kr_r) # upper confidence bound 
x_p_teor <- qweibull(p, shape=nu, scale = eta, lower.tail = TRUE) 
data.frame(p, x_p_teor, x_p_iv, Q, x_p_l, x_p_u)
```

And for the 0.9 quantile.

```{r}
library(msm)
eta_iv=rez_optim$par[1] #estimator of eta
nu_iv=rez_optim$par[2] #estimator of nu
p <- 0.9 # for median p=0.5 should be taken Q <- 0.95 #confidence level
alpha <- 1-Q
Fisher_inf<- (-1)*rez_optim$hessian
Fisher_inv<-solve(Fisher_inf)
sd_gamma <- deltamethod(list(~ log(x1)+log(-log(0.5))/x2), rez_optim$par, Fisher_inv)
sd_gamma
kr_r <- qnorm(alpha/2, mean = 0, sd = 1, lower.tail = FALSE) #alpha/2 critical # value z_{alpha/2} of the standard normal law
x_p_iv <- qweibull(p, shape=nu_iv, scale =
eta_iv, lower.tail = TRUE) # point estimate of
#p-quantile (median, in particular).
x_p_l <- x_p_iv*exp((-1)*sd_gamma*kr_r) # lower confidence bound
x_p_u <- x_p_iv*exp(sd_gamma*kr_r) # upper confidence bound 
x_p_teor <- qweibull(p, shape=nu, scale = eta, lower.tail = TRUE) 
data.frame(p, x_p_teor, x_p_iv, Q, x_p_l, x_p_u)
```

## Task 1.3

### maxLik

```{r}
set.seed(12)
k <- 1
p <- 0.7
n <- 50
t <- rbinom(n, k, p)
t
#\begin{verbatim}
library(maxLik)
ln_l2 <- function(par) {
sum (t*log(par[1])+(k-t)*log(1-par[1])) }
mle <- maxLik(logLik = ln_l2, start = c(0.5))
coef(mle)
```

### optim

```{r}
init<-c(p=0.5)
rez_optim <- optim(init, fn=ln_l2, method="L-BFGS-B", lower = c(1.0e-13), upper=c(0.99999999), control=list(fnscale=-1), hessian=TRUE)
rez_optim$par
rez_optim$hessian
```

## Task 1.4

```{r}
library("xlsx")
# Import (RStudio select Import Dataset) data into dataframe Duom1.
duom1<-read.table(file = "dataset1.txt", header = TRUE) #You take dataset1 from your own depository.
duom1<- as.data.frame(duom1)
k=11
library(maxLik)
ln_l3 <- function(par) {
  eta <- par[1]
  nu <- par[2]
  reziai=k-1
  f=duom1$U[1]*log(pweibull(
    duom1$Right[1],
    shape=par[2],
    scale=par[1],
    lower.tail = TRUE)) +
  duom1$U[k]*log(1-pweibull(
    duom1$Left[k],
    shape=par[2],
    scale = par[1],
    lower.tail = TRUE)) +
  sum(duom1$U[2:reziai]*log(pweibull(
    duom1$Right[2:reziai],
    shape=par[2],
    scale = par[1],
    lower.tail = TRUE) -
  pweibull(
    duom1$Left[2:reziai],
    shape=par[2],
    scale = par[1],
    lower.tail = TRUE)))
return(f)
}
mle <- maxLik(logLik = ln_l3, start = c(650,2))
coef(mle)
```

## Task 1.5

```{r}
set.seed(12)
eta <- 10
nu <- 2
n <- 100
t <- eta*(((-1)*log(1-runif(n)))^(1/nu))
library(MASS)
fitdistr(t, "Weibull")
```

## Task 1.6

```{r}
set.seed(12)
eta <- 10
nu <- 2
n <- 100
t <- eta*(((-1)*log(1-runif(n)))^(1/nu))# generates T_i t

hist(t)
```

```{r}
library(fitdistrplus)
fitW <- fitdist(t, "weibull")
fitg <- fitdist(t, "gamma")
fitln <- fitdist(t, "lnorm")

summary(fitW)
summary(fitg)
summary(fitln)
```

```{r}
gofstat(list(fitW, fitg, fitln), fitnames=c("Weibull", "gamma",
"lognormal"))
```

```{r}
denscomp(list(fitW, fitg, fitln), legendtext=c("Weibull", "gamma",
"lognormal"))
```

```{r}
cdfcomp(list(fitW, fitg, fitln), legendtext=c("Weibull", "gamma",
"lognormal"))
```

```{r}
cdfcomp(list(fitW, fitg), legendtext=c("Weibull", "gamma"))
```

```{r}
qqcomp(list(fitW, fitg, fitln), legendtext=c("Weibull", "gamma",
"lognormal"))
ppcomp(list(fitW, fitg, fitln), legendtext=c("Weibull", "gamma",
"lognormal"))
```

## Task 1.6.b

```{r}
set.seed(12)
x<- rnorm(n,200,10)
hist(x)
```

```{r}
fitnor <- fitdist(x, "norm")
fitlogis <- fitdist(x, "logis")
fitcauchy<- fitdist(x, "cauchy")
summary(fitnor)
summary(fitlogis)
summary(fitcauchy)
```

```{r}
gofstat(list(fitnor, fitlogis,fitcauchy), fitnames=c("Normal", "Logistic", "Cauchy"))
```

```{r}
denscomp(list(fitnor, fitlogis,fitcauchy), legendtext=c("Normal", "Logistic", "Cauchy"))
```

```{r}
cdfcomp(list(fitnor, fitlogis,fitcauchy), legendtext=c("Normal", "Logistic","Cauchy"))
```

```{r}
qqcomp(list(fitnor, fitlogis,fitcauchy), legendtext=c("Normal", "Logistic","Cauchy"))
ppcomp(list(fitnor, fitlogis,fitcauchy), legendtext=c("Normal", "Logistic","Cauchy"))
```

## Task 1.6.c

```{r}
set.seed(1)
x <- rnbinom(100,5,0.2)# generating negative binomial data
fitnbinom <- fitdist(x, "nbinom")# fitting the negative binomial model 
fitpois <- fitdist(x, "pois")# fitting the Poisson model
fitnbinom
fitpois
```

```{r}
plot(fitpois)
```

```{r}
plot(fitnbinom)
```

```{r}
cdfcomp(list(fitpois, fitnbinom),legendtext = c("Poisson", "negative binomial"))
```

